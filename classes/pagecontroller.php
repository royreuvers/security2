<?php

class PageController
{
    function getEncrypt(){
        include_once("views/encrypt.php");
    }

    function getInloggen(){
        include_once("views/inloggen.php");
    }

    function getBericht(){
        include_once("views/bericht.php");
    }

    function getUitloggen(){
        session_start();
        session_destroy();
        include_once("views/encrypt.php");
    }

    function postEncrypt(){
        session_start();
        $servername = "localhost";
        $username = "u317892411_enc";
        $password = "royred2217";
        $dbname = "u317892411_enc";

        if(!empty($_POST["naam"]) && !empty($_POST["wachtwoord"]) && !empty($_POST["bericht"])) {
            $conn = new mysqli($servername, $username, $password, $dbname);

            $naam = $conn->real_escape_string($_POST["naam"]);
            $wachtwoord = $conn->real_escape_string($_POST["wachtwoord"]);
            $bericht = $conn->real_escape_string($_POST["bericht"]);

            $salt = "Dit is een encryptie tekst voor het wachtwoord";
            $wachtwoord = md5($salt.$wachtwoord.$salt);

            $sql = "INSERT INTO encrypties (naam, wachtwoord, bericht) VALUES ('$naam', '$wachtwoord', aes_encrypt('$bericht','12345'))";

            if ($conn->query($sql) === TRUE) {
                header("Location: http://avansencryption.esy.es/inloggen");
            } else {
                header("Location: http://avansencryption.esy.es");
            }

            $conn->close();
        }
        else{
            header("Location: http://avansencryption.esy.es");
        }
    }

    function postInloggen(){
        session_start();
        $servername = "localhost";
        $username = "u317892411_enc";
        $password = "royred2217";
        $dbname = "u317892411_enc";

        if(!empty($_POST["naam"]) && !empty($_POST["wachtwoord"])) {
            $conn = new mysqli($servername, $username, $password, $dbname);

            $naam = $conn->real_escape_string($_POST["naam"]);
            $wachtwoord = $conn->real_escape_string($_POST["wachtwoord"]);

            $salt = "Dit is een encryptie tekst voor het wachtwoord";
            $wachtwoord = md5($salt.$wachtwoord.$salt);

            $sql = "SELECT CAST(AES_DECRYPT(bericht, '12345') AS CHAR(255)) bericht FROM encrypties WHERE naam = '$naam' AND wachtwoord='$wachtwoord'";

            $result = mysqli_query($conn, $sql);
            $rows = mysqli_num_rows($result);
            if($rows > 0) {
                $row = $result->fetch_array();
                $_SESSION["ingelogd"] = true;
                $_SESSION["bericht"] = $row["bericht"];
                header("Location: http://avansencryption.esy.es/bericht");
            }
            else {
                header("Location: http://avansencryption.esy.es/inloggen");
            }
            $conn->close();
        }
        else{
            header("Location: http://avansencryption.esy.es/inloggen");
        }
    }
}
