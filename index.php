<?php

require_once("classes/altorouter.php");
require_once("classes/pagecontroller.php");

$altoRouter = new AltoRouter();

$altoRouter->map('GET', '/', function () {
	$pageController = new PageController();
	$pageController->getEncrypt();
});

$altoRouter->map('GET', '/inloggen', function () {
	$pageController = new PageController();
	$pageController->getInloggen();
});

$altoRouter->map('GET', '/bericht', function () {
	$pageController = new PageController();
	$pageController->getBericht();
});

$altoRouter->map('GET', '/uitloggen', function () {
	$pageController = new PageController();
	$pageController->getUitloggen();
});


$altoRouter->map('POST', '/encrypt', function () {
	$pageController = new PageController();
	$pageController->postEncrypt();
});

$altoRouter->map('POST', '/inloggen', function () {
	$pageController = new PageController();
	$pageController->postInloggen();
});

$match = $altoRouter->match();

if ($match && is_callable($match['target'])) {
	call_user_func_array($match['target'], $match['params']);
}